FROM node:10.15.3-alpine

LABEL version="0.1"
LABEL description="Customer service for Kubo"
LABEL maintainer="aziztux[at]gmail"

WORKDIR /usr/src/app
COPY src .
RUN npm install

EXPOSE 3000
CMD [ "npm", "start" ]