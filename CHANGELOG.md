# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]
### Added
- New structure based on the original Java application
- Support for a MongoDB database hosted on Mongo Atlas (using mongoose)
- Docker support
### Changed
- Documentation
### Removed