var express = require('express');
var router = express.Router();

// Require our controllers.
var controller = require('./controller');

// POST request for creating Customer.
router.post('/customers/create', controller.create);

// DELETE request to delete Customer
router.delete('/customers/delete/:id', controller.delete);

// PUT request to update Customer.
router.put('/customers/update/:id', controller.update);

// GET request for one Customer.
router.get('/customers/get/:id', controller.detail);

// GET request for list of all Customers.
router.get('/customers/get', controller.list);

module.exports = router;