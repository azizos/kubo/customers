const express = require('express');
const router = require('./routes.js');
const logger = require('morgan');
const helmet = require('helmet');
const dotenv = require('dotenv');
const compression = require('compression');
const db = require('./utils/db.js');

dotenv.config();
const port = 3000;
const mongoUser = process.env.MONGO_USER || MONGO_USER;
const mongoPswd = process.env.MONGO_PSWD || MONGO_PSWD;
const mongoHost = process.env.MONGO_HOST || MONGO_HOST;
const mongoURL = "mongodb+srv://"+mongoUser+":"+mongoPswd+"@"+mongoHost+"/customers?retryWrites=true"

db.connect(mongoURL);
const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(helmet());
app.use(compression());
app.use('/', router);

app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);
  res.send('error');
});

app.listen(port, () => console.log(`Customer service is listening on port ${port}!`))
module.exports = app;