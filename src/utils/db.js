const mongoose = require('mongoose');

exports.connect = (connectionString) => {
    mongoose.connect(connectionString, { useNewUrlParser: true });
    mongoose.Promise = global.Promise;
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error'));
};