var Customer = require('./model.js');
var async = require('async');
var request = require("request");

const { body, validationResult } = require('express-validator/check');
const { sanitizeBody } = require('express-validator/filter');

// Handle Customer create on POST.
exports.create = [
    // Validate fields.
    body('first_name').isLength({ min: 1 }).trim().withMessage('First name must be specified.')
        .isAlphanumeric().withMessage('First name has non-alphanumeric characters.'),
    body('last_name').isLength({ min: 1 }).trim().withMessage('Last name must be specified.')
        .isAlphanumeric().withMessage('Family name has non-alphanumeric characters.'),
    body('email').isLength({ min: 1 }).trim().withMessage('Email must be specified.'),

    // Sanitize fields.
    sanitizeBody('first_name').escape(),
    sanitizeBody('last_name').escape(),
    sanitizeBody('email').escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create Customer object with escaped and trimmed data
        var customer = new Customer(
            {
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                email: req.body.email
            }
        );
        if (!errors.isEmpty()) {
            res.status(400);
            res.send(errors.array())
            return;
        }
        else {
            Customer.countDocuments({ email: customer.email }, function (err, count) {
                if (count > 0) {
                    res.status(400);
                    res.send({ "message": "Please choose another email" });
                } else {
                    customer.save(function (err) {
                        if (err) { return next(err); }
                        res.send(customer)
                    });
                }
            });
        }
    }
];

exports.delete = function (req, res, next) {
    console.log(req.params.id);
    Customer.findByIdAndRemove(req.params.id, function(err) {
        if (err) { return next(err); }
        res.status(200);
        res.redirect('/customers/get');
    });
}

exports.update = [

    // Validate fields.
    body('email').isLength({ min: 1 }).trim().withMessage('Email must be specified.'),

    sanitizeBody('email').escape(),

    // Process request after validation and sanitization.
    (req, res, next) => {
        // Extract the validation errors from a request.
        const errors = validationResult(req);

        // Create Author object with escaped and trimmed data (and the old id!)
        var customer = new Customer(
            {
                email: req.body.email,
                _id: req.params.id

            }
        );
        if (!errors.isEmpty()) {
            res.status(400);
            res.send(errors.array())
            return;
        }
        else {
            Customer.countDocuments({ email: customer.email }, function (err, count) {
                if (count > 0) {
                    res.status(400);
                    res.send({ "message": "Please choose another email" });
                } else {
                    Customer.findByIdAndUpdate(req.params.id, customer, { new: true }, function (err, thecustomer) {
                        if (err) { return next(err); }
                        res.send(thecustomer);
                    });
                }
            });
        }
    }
];

exports.detail = function (req, res, next) {
    async.parallel({
        customer: function (callback) {
            Customer.findById(req.params.id)
                .exec(callback)
        },
    }, function (err, results) {
        if (results.customer == null) {
            res.statusCode = 404;
            res.end(JSON.stringify({ "message": "Customer not found!" }));
        }
        res.statusCode = 200;
        res.end(JSON.stringify(results.customer))
    });
}

exports.list = function (req, res, next) {
    Customer.find()
        .exec(function (err, customer_list) {
            if (err) { return next(err); }
            res.status(200)
            res.send(customer_list);
        });
}